export class User {
    id: number;
    name: string;
    profile: string;
    address: string;
    constructor() {
        this.id = 0;
        this.name = "";
        this.profile = "";
        this.address = "";
    }
}