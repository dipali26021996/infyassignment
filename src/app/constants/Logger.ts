import { Component } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Injectable } from '@angular/core';
import { JsonFormatter } from 'tslint/lib/formatters';
@Injectable({
  providedIn: 'root',
})
export class Logger {
  constructor(private logger: NGXLogger) {}

  public info(log: any) {
    this.logger.info(log);
  }

  public logData(log: any, data: any) {
    this.logger.info(log + ' = ' + JSON.stringify(data));
  }
  public error(log: any) {
    this.logger.error(log);
  }

  public debug(log: any) {
    this.logger.debug(log);
  }
}
