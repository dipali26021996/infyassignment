import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { GetDetailsComponent } from './get-details/get-details.component';
import { UserinfoComponent } from './userinfo/userinfo.component';

const routes: Routes = [
  {path : 'getdetails', component: GetDetailsComponent},
  {path : 'editDetails', component: EditDetailsComponent},
  {path : 'userinfo', component: UserinfoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  GetDetailsComponent,
  EditDetailsComponent,
  UserinfoComponent
]
