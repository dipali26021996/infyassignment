import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../constants/User';

@Component({
  selector: 'app-get-details',
  templateUrl: './get-details.component.html',
  styleUrls: ['./get-details.component.css']
})
export class GetDetailsComponent implements OnInit {
  user: User;
  users: Array<User>;
  constructor(private router: Router) {
    this.user = new User();
    this.users = [];
  }
  ngOnInit(): void {
    if (!("users" in history.state)) {
      this.getInitArray();
    } else {
      this.users = JSON.parse(JSON.stringify(history.state.users));
     
    }
  }
  editUser(user: any) {
    this.user = JSON.parse(JSON.stringify(user));
    localStorage.setItem("user", JSON.stringify(this.user));
    if (confirm('Do you want to edit this details?')) {
      this.router.navigateByUrl('/editDetails')
    }
  };
  deleteUser(user: any) {
    this.user = JSON.parse(JSON.stringify(user));
    if (confirm('Do you want to delete this details?')) {
      this.removeItem(this.user)
    }
  };
  removeItem(obj: User) {
    const index: number = this.getIndex(this.users, obj.id);
    this.users.splice(index, 1);
  }


  getIndex(users: Array<User>, id: number) {
    for (var i = 0; i < users.length; i++) {
      if (users[i].id == id) {
        return i;
      }
    }
    return -1;
  }

  getInitArray() {
    this.user = new User();
    this.user.id = 1;
    this.user.name = "Deepika";
    this.user.profile = "JAVA DEV";
    this.user.address = "NAGPUR";
    this.users[0] = this.user;


    this.user = new User();
    this.user.id = 2;
    this.user.name = "Radha";
    this.user.profile = "JAVA DEV";
    this.user.address = "Pune";
    this.users[1] = this.user;


    this.user = new User();
    this.user.id = 3;
    this.user.name = "Rohini  ";
    this.user.profile = "JAVA DEV";
    this.user.address = "Delhi";
    this.users[2] = this.user;


    this.user = new User();
    this.user.id = 4;
    this.user.name = "Deepali";
    this.user.profile = "Web DEV";
    this.user.address = "Kheda";
    this.users[3] = this.user;

    localStorage.setItem("users", JSON.stringify(this.users));
  }

}
