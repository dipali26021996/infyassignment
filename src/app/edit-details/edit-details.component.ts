import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../constants/User';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Logger } from '../constants/Logger';
@Component({
  selector: 'app-edit-details',
  templateUrl: './edit-details.component.html',
  styleUrls: ['./edit-details.component.css']
})
export class EditDetailsComponent implements OnInit {
  users: Array<User>;
  submitted = false;
  user: User;
  registerForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    profile: new FormControl('')
  });

  constructor(private router: Router,
    private formBuilder: FormBuilder, private store: Store<any>
  ) {
    this.user = new User();
    this.users = [];
  }



  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user') || '{}');
    this.users = JSON.parse(localStorage.getItem('users') || '[]');

    // //validation rules
    this.registerForm = this.formBuilder.group({
      name: [this.user.name, Validators.required],
      address: [this.user.address, Validators.required],
      profile: [this.user.profile, Validators.required],
    });
  }

  updateUser() {
    this.mapDataObject();
    this.submitted = true;
    //stop here if form is invalid
    if (this.registerForm.invalid) {
      console.log('Validation Fail');
      return;
    }
    console.log('Validation Pass');
    this.users[this.getIndex(this.users,this.user.id)] = this.user;
    this.router.navigateByUrl('/getdetails', {
      state: { users: this.users },
    });

  }
  get f() {
    return this.registerForm.controls;
  }

  mapDataObject() {
    this.user = Object.assign(this.user, this.registerForm.value);
    return this.user;
  }
  getIndex(users: Array<User>, id: number) {
    for (var i = 0; i < users.length; i++) {
       if (users[i].id == id) {
        return i;
      } 
    }
    return 0;
  }
}
